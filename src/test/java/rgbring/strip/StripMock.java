package rgbring.strip;

import java.util.stream.IntStream;

import rgbring.color.Color;

public class StripMock implements Strip {

	private static final Color OFF = Color.BLACK;

	private final Color[] colors;

	public StripMock(int leds) {
		this.colors = IntStream.range(0, leds).mapToObj(i -> OFF).toArray(Color[]::new);
	}

	@Override
	public int leds() {
		return colors.length;
	}

	@Override
	public void setColor(int led, int r, int g, int b) {
		this.colors[led] = new Color(r, g, b);
	}

	public Color getColor(int led) {
		return colors[led];
	}

	public Color[] colors() {
		return colors.clone();
	}

}
