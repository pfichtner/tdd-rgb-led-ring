package rgbring;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static rgbring.color.Color.BLACK;
import static rgbring.color.Color.GREEN;
import static rgbring.color.Color.ORANGE;
import static rgbring.color.Color.YELLOW;

import org.junit.Test;

import rgbring.color.Color;
import rgbring.strip.Strip;
import rgbring.strip.StripMock;
import rgbring.strip.SubStrip;

public class VumeterMultiTest {

	private static final Color OFF = BLACK;
	private static final int RINGS = 2;
	private static final int LEDS_PER_RING = 3;

	private StripMock strip = new StripMock(RINGS * LEDS_PER_RING);
	private AbstractRgbRing ring1 = new RgbRing(offset(strip, offset1(), LEDS_PER_RING));
	private AbstractRgbRing ring2 = new RgbRing(offset(strip, offset2(), LEDS_PER_RING));

	private int offset1() {
		return 0;
	}

	private int offset2() {
		return ring1.leds();
	}

	@Test
	public void whenUsingRing1_RightSiteStaysOff() {
		givenLevel(ring1, 100);
		thenStripIs(GREEN, YELLOW, ORANGE, OFF, OFF, OFF);
	}

	@Test
	public void whenUsingRing2_LeftSiteStaysOff() {
		givenLevel(ring2, 100);
		thenStripIs(OFF, OFF, OFF, GREEN, YELLOW, ORANGE);
	}

	private Strip offset(Strip delegate, int offset, int length) {
		return new SubStrip(delegate, offset, length);
	}

	private void givenLevel(AbstractRgbRing ring, int level) {
		ring.setLevel(level);
	}

	private void thenStripIs(Color... colors) {
		assertThat(strip.colors(), is(colors));
	}

}
