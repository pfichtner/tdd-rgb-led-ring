package rgbring.junit.rules;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.rules.ExternalResource;

public class MqttClient extends ExternalResource {

	public static class Message {

		private String topic;
		private String payload;

		public Message(String topic, byte[] payload) {
			this.topic = topic;
			this.payload = new String(payload);
		}

		public String getTopic() {
			return topic;
		}

		public String getPayload() {
			return payload;
		}

	}

	private String clientId = "testclient";

	private final List<String> subscriptions = new ArrayList<>();
	private final List<MqttClient.Message> messages = new ArrayList<>();
	private final org.eclipse.paho.client.mqttv3.MqttClient mqttClient;

	public static MqttClient mqttClientConnectedTo(MqttBroker mqttBroker) {
		return mqttClientConnectedTo(mqttBroker.getHostname(),
				mqttBroker.getPort());
	}

	public static MqttClient mqttClientConnectedTo(String hostname, int port) {
		return new MqttClient(hostname, port);
	}

	private MqttClient(String hostname, int port) {
		try {
			mqttClient = new org.eclipse.paho.client.mqttv3.MqttClient(
					String.format("tcp://%s:%s", hostname, port), clientId);
			mqttClient.setCallback(newCallback());
		} catch (MqttException e) {
			throw new RuntimeException(e);
		}
	}

	protected MqttCallback newCallback() {
		return new MqttCallback() {

			@Override
			public void messageArrived(String topic, MqttMessage message)
					throws Exception {
				messages.add(new Message(topic, message.getPayload()));
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken deliveryToken) {
				// TODO Auto-generated method stub
			}

			@Override
			public void connectionLost(Throwable throwable) {
				// TODO Auto-generated method stub
			}

		};
	}

	@Override
	protected void before() throws Throwable {
		super.before();
		mqttClient.connect();
		mqttClient.subscribe(subscriptions.toArray(new String[subscriptions
				.size()]));
	}

	@Override
	protected void after() {
		try {
			mqttClient.disconnect();
			mqttClient.close();
		} catch (MqttException e) {
			throw new RuntimeException(e);
		}
		super.after();
	}

	public MqttClient withSubscriptionTo(String topic) {
		this.subscriptions.add(topic);
		return this;
	}

	public MqttClient withWildcardSubscriptionTo(String topic) {
		return withSubscriptionTo(topic + "#");
	}

	public List<MqttClient.Message> getMessages() {
		return new ArrayList<>(messages);
	}

}