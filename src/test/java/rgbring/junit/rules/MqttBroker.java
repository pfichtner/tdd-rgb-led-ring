package rgbring.junit.rules;

import org.junit.rules.ExternalResource;

public class MqttBroker extends ExternalResource {

	private String hostname = "iot.eclipse.org";
	private int port = 1883;

	public static MqttBroker mqttBroker() {
		// TODO start a broker locally and use localhost
		return mqttBroker("localhost", 1883);
	}

	public static MqttBroker mqttBroker(String hostname, int port) {
		return new MqttBroker(hostname, port);
	}

	private MqttBroker(String hostname, int port) {
		this.hostname = hostname;
		this.port = port;
	}

	public String getHostname() {
		return hostname;
	}

	public int getPort() {
		return port;
	}

	@Override
	protected void before() throws Throwable {
		super.before();
	}

	@Override
	protected void after() {
		super.after();
	}

}
