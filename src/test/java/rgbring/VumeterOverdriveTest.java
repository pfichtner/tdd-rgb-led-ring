package rgbring;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static rgbring.color.Color.GREEN;
import static rgbring.color.Color.ORANGE;
import static rgbring.color.Color.RED;
import static rgbring.color.Color.YELLOW;

import java.util.stream.IntStream;

import org.junit.Test;

import rgbring.AbstractRgbRing;
import rgbring.RgbRing;
import rgbring.color.Color;
import rgbring.decorates.Overdrive;
import rgbring.strip.StripMock;

public class VumeterOverdriveTest {

	private static final int OVERDRIVE_LEVEL = 91;
	private static final Color OVERDRIVE_COLOR = RED;

	private AbstractRgbRing ring;
	private StripMock strip;

	@Test
	public void allLedsAreInDefaultColorsIfOverlevelLimitIsHit() {
		givenLeds(3);
		whenLevelIs(OVERDRIVE_LEVEL - 1);
		thenLedsAre(GREEN, YELLOW, ORANGE);
	}

	@Test
	public void allLedsAreInDefaultColorsIfOverlevelLimitIsExceeded() {
		givenLeds(3);
		whenLevelIs(OVERDRIVE_LEVEL);
		thenCompleteRingHas(OVERDRIVE_COLOR);
	}

	private Color[] times(int times, Color color) {
		return IntStream.range(0, times).mapToObj(i -> color).toArray(Color[]::new);
	}

	public void thenCompleteRingHas(Color color) {
		thenLedsAre(times(ring.leds(), color));
	}

	private void thenLedsAre(Color... colors) {
		assertThat(strip.colors(), is(colors));
	}

	private void whenLevelIs(int level) {
		ring.setLevel(level);
	}

	private void givenLeds(int leds) {
		this.strip = new StripMock(leds);
		this.ring = overdrive(new RgbRing(strip));
	}

	private AbstractRgbRing overdrive(AbstractRgbRing delegate) {
		return new Overdrive(delegate, OVERDRIVE_LEVEL, OVERDRIVE_COLOR);
	}

}
