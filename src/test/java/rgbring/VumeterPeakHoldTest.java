package rgbring;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static rgbring.color.Color.BLACK;
import static rgbring.color.Color.GREEN;
import static rgbring.color.Color.ORANGE;
import static rgbring.color.Color.YELLOW;

import java.util.Arrays;

import org.junit.Test;

import rgbring.color.Color;
import rgbring.decorates.PeakHold;
import rgbring.decorates.PeakHoldDropping;
import rgbring.strip.StripMock;

public class VumeterPeakHoldTest {

	private static final Color OFF = BLACK;

	private AbstractRgbRing ring;
	private StripMock strip;

	protected int previousPeak;

	@Test
	public void peakHoldIsResetAfterThreeTimes() {
		givenLeds(9);
		ring.setLevel(100);

		ColorBuilder level10WithPeak = fromStrip().replaceAll(1, OFF).set(8, ORANGE);
		whenLevelIs(10);
		thenLedsAre(level10WithPeak);

		whenLevelIs(10);
		thenLedsAre(level10WithPeak);

		whenLevelIs(10);
		thenLedsAre(level10WithPeak);

		whenLevelIs(10);
		thenLedsAre(level10WithPeak.set(8, OFF));
	}

	@Test
	public void peakIncreasesWhenLevelIncreases() {
		givenLeds(9);

		ColorBuilder seven = new ColorBuilder(GREEN, GREEN, GREEN, YELLOW, YELLOW, YELLOW, ORANGE, OFF, OFF);

		levelThatResultInLedsOn(7);
		thenLedsAre(seven);

		ColorBuilder firstGreen = seven.replaceAll(1, OFF);

		for (int i = 0; i < 2; i++) {
			levelThatResultInLedsOn(1);
			thenLedsAre(firstGreen.set(6, ORANGE));
		}

		levelThatResultInLedsOn(8);
		thenLedsAre(seven.set(7, ORANGE));

		for (int i = 0; i < 3; i++) {
			levelThatResultInLedsOn(1);
			thenLedsAre(firstGreen.set(7, ORANGE));
		}

		levelThatResultInLedsOn(1);
		thenLedsAre(firstGreen);
	}

	@Test
	public void peakHoldDrops() {
		givenLedsWithDroppingPeak(9);
		whenLevelIs(100);
		Color[] allColors = strip.colors();
		int peakAt = strip.leds() - 1;

		ColorBuilder level10 = fromStrip().replaceAll(1, OFF);
		for (int i = 0; i < 3; i++) {
			whenLevelIs(10);
			thenLedsAre(level10.replaceAll(1, OFF).set(peakAt, ORANGE));
		}

		while (peakAt > 0) {
			whenLevelIs(10);
			thenLedsAre(level10.set(--peakAt, allColors[peakAt]));
		}
	}

	private ColorBuilder fromStrip() {
		return new ColorBuilder(strip.colors());
	}

	private static class ColorBuilder {

		private final Color[] colors;

		public ColorBuilder(Color... colors) {
			this.colors = colors.clone();
		}

		private ColorBuilder set(int idx, Color color) {
			ColorBuilder clone = new ColorBuilder(colors);
			clone.colors[idx] = color;
			return clone;
		}

		private ColorBuilder replaceAll(int startIdx, Color color) {
			ColorBuilder clone = new ColorBuilder(colors);
			Arrays.fill(clone.colors, startIdx, clone.colors.length, color);
			return clone;
		}

		private Color[] build() {
			return colors.clone();
		}
	}

	private void thenLedsAre(ColorBuilder colorBuilder) {
		thenLedsAre(colorBuilder.build());
	}

	private void thenLedsAre(Color... colors) {
		assertThat(strip.colors(), is(colors));
	}

	private void levelThatResultInLedsOn(int count) {
		whenLevelIs(100 * count / strip.leds());
	}

	private void whenLevelIs(int level) {
		ring.setLevel(level);
	}

	private void givenLeds(int leds) {
		this.strip = new StripMock(leds);
		this.ring = new PeakHold(new RgbRing(this.strip), 3);
	}

	private void givenLedsWithDroppingPeak(int leds) {
		this.strip = new StripMock(leds);
		this.ring = new PeakHoldDropping(new RgbRing(this.strip), 3);
	}

}
