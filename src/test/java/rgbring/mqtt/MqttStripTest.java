package rgbring.mqtt;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.rules.RuleChain.outerRule;
import static rgbring.junit.rules.MqttBroker.mqttBroker;
import static rgbring.junit.rules.MqttClient.mqttClientConnectedTo;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import rgbring.junit.rules.MqttBroker;
import rgbring.junit.rules.MqttClient;
import rgbring.junit.rules.MqttClient.Message;

public class MqttStripTest {

	private static final String TOPIC = "some/led/";

	// TODO do not depend on iot.eclipse.org but start a broker locally
	// private MqttBroker mqttBroker = mqttBroker();
	private MqttBroker mqttBroker = mqttBroker("iot.eclipse.org", 1883);
	private MqttClient mqttClient = mqttClientConnectedTo(mqttBroker)
			.withWildcardSubscriptionTo(TOPIC);

	@Rule
	public TestRule chain = outerRule(mqttBroker).around(mqttClient);

	private MqttStrip sut;

	@Before
	public void setup() throws MqttException {
		sut = new MqttStrip(anyPositiveLength(), mqttBroker.getHostname(),
				mqttBroker.getPort());
	}

	@Test
	public void sutDoesSendMessageToBroker() throws MqttException,
			InterruptedException {
		int led = 1;
		sut.setColor(led, 0, 128, 255);
		Iterator<Message> messages = waitForMessageReceived(5, SECONDS);
		assertContains(messages, message(TOPIC + led + "/rgb", "#0080FF"));
		assertIsEmpty(messages);
	}

	private Iterator<Message> waitForMessageReceived(int duration,
			TimeUnit timeUnit) throws InterruptedException {
		long until = System.currentTimeMillis() + timeUnit.toMillis(duration);
		List<Message> messages;
		while ((messages = mqttClient.getMessages()).isEmpty()) {
			if (System.currentTimeMillis() > until) {
				throw new IllegalStateException("no message received");
			}
			TimeUnit.MILLISECONDS.sleep(10);
		}
		return messages.iterator();
	}

	private void assertIsEmpty(Iterator<Message> messages) {
		assertThat("messages not empty", messages.hasNext(), is(false));
	}

	private Message message(String topic, String payload) {
		return new Message(topic, payload.getBytes());
	}

	private void assertContains(Iterator<Message> iterator, Message expected) {
		assertThat("messages empty", iterator.hasNext(), is(true));
		Message message = iterator.next();
		assertThat(message.getTopic(), is(expected.getTopic()));
		assertThat(message.getPayload(), is(expected.getPayload()));
	}

	private int anyPositiveLength() {
		return 3;
	}

}
