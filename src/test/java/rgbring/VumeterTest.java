package rgbring;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static rgbring.color.ArrayColorSchema.builder;
import static rgbring.color.Color.BLACK;
import static rgbring.color.Color.GREEN;
import static rgbring.color.Color.ORANGE;
import static rgbring.color.Color.YELLOW;

import org.junit.Test;

import rgbring.color.ArrayColorSchema;
import rgbring.color.ArrayColorSchema.Builder;
import rgbring.color.Color;
import rgbring.strip.StripMock;

public class VumeterTest {

	private static final Color OFF = BLACK;
	private AbstractRgbRing ring;
	private StripMock strip;

	@Test
	public void whenLevelIsZeroAllLedsAreTurnedOff() {
		givenStripWithLeds(1);
		whenLevelIs(0);
		thenLedsAre(OFF);
	}

	@Test
	public void givenTwoLedsAndLevel1Led0IsTurnedOn() {
		givenStripWith(leds(1, GREEN).and(1, ORANGE));
		whenLevelIs(1);
		thenLedsAre(GREEN, OFF);
	}

	@Test
	public void givenTwoLedsAndLevel51Led1IsOrange() {
		givenStripWith(leds(1, GREEN).and(1, ORANGE));
		whenLevelIs(51);
		thenLedsAre(GREEN, ORANGE);
	}

	@Test
	public void givenThreeLedsAndLevel1Led0IsGreen() {
		givenStripWithLeds(3);
		whenLevelIs(1);
		thenLedsAre(GREEN, OFF, OFF);
	}

	@Test
	public void givenThreeLedsAndLevel34Led2IsYellow() {
		givenStripWithLeds(3);
		whenLevelIs(34);
		thenLedsAre(GREEN, YELLOW, OFF);
	}

	@Test
	public void givenThreeLedsAndLevel67Led3IsTurnedOn() {
		givenStripWithLeds(3);
		whenLevelIs(67);
		thenLedsAre(GREEN, YELLOW, ORANGE);
	}

	@Test
	public void givenSixLedsAndLevel100AllLedsAreColorOrange() {
		givenStripWithLeds(6);
		whenLevelIs(100);
		thenLedsAre(GREEN, GREEN, YELLOW, YELLOW, ORANGE, ORANGE);
	}

	@Test
	public void givenElevenLedsAndLevel100AllLedsAreColorOrange() {
		givenStripWith(leds(4, GREEN).and(3, YELLOW).and(4, ORANGE));
		whenLevelIs(100);
		thenLedsAre(GREEN, GREEN, GREEN, GREEN, YELLOW, YELLOW, YELLOW, ORANGE, ORANGE, ORANGE, ORANGE);
	}

	private Builder leds(int times, Color color) {
		return builder().and(times, color);
	}

	private void thenLedsAre(Color... colors) {
		assertThat(strip.colors(), is(colors));
	}

	private void whenLevelIs(int level) {
		ring.setLevel(level);
	}

	private void givenStripWithLeds(int leds) {
		this.strip = new StripMock(leds);
		this.ring = new RgbRing(strip);
	}

	public void givenStripWith(ArrayColorSchema.Builder builder) {
		ArrayColorSchema colorSchema = builder.build();
		int leds = colorSchema.size();
		this.strip = new StripMock(leds);
		this.ring = new RgbRing(strip, colorSchema);
	}

}
