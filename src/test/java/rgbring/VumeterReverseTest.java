package rgbring;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static rgbring.color.Color.BLACK;
import static rgbring.color.Color.GREEN;
import static rgbring.color.Color.ORANGE;
import static rgbring.color.Color.YELLOW;

import org.junit.Before;
import org.junit.Test;

import rgbring.AbstractRgbRing;
import rgbring.RgbRing;
import rgbring.color.Color;
import rgbring.decorates.Swapped;
import rgbring.strip.Strip;
import rgbring.strip.StripMock;
import rgbring.strip.SubStrip;

public class VumeterReverseTest {

	private static final Color OFF = BLACK;

	private StripMock strip;

	// TODO this test SHOWS nicely GREEN,YELLOW,ORANGE,ORANGE,YELLOW,GREEN but
	// does not really need TWO rings
	private AbstractRgbRing ring1;
	private AbstractRgbRing ring2;

	@Before
	public void setup() {
		int stripSize = 6;
		strip = new StripMock(stripSize);
		int ledsPerRing = strip.leds() / 2;
		ring1 = new RgbRing(offset(strip, 0, ledsPerRing));
		ring2 = swap(new RgbRing(offset(strip, ring1.leds())));
	}

	@Test
	public void whenUsingRing1_RightHalfOfStripIsOff() {
		ring1.setLevel(100);
		thenLedsAre(GREEN, YELLOW, ORANGE, OFF, OFF, OFF);
	}

	@Test
	public void whenUsingRing2_RightHalfOfStripIsOff() {
		ring2.setLevel(100);
		thenLedsAre(OFF, OFF, OFF, ORANGE, YELLOW, GREEN);
	}

	private Strip offset(Strip delegate, int offset) {
		return new SubStrip(delegate, offset);
	}

	private Strip offset(Strip delegate, int offset, int ledPerRing) {
		return new SubStrip(delegate, offset, ledPerRing);
	}

	private AbstractRgbRing swap(AbstractRgbRing delegate) {
		return new Swapped(delegate);
	}

	private void thenLedsAre(Color... colors) {
		assertThat(strip.colors(), is(colors));
	}

}
