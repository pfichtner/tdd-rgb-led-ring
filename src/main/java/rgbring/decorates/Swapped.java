package rgbring.decorates;

import rgbring.AbstractRgbRing;

public final class Swapped extends AbstractRgbDelegate {

	public Swapped(AbstractRgbRing delegate) {
		super(delegate);
	}

	@Override
	public void setColor(int led, int r, int g, int b) {
		delegate().setColor(swapLedNumber(led), r, g, b);
	}

	private int swapLedNumber(int led) {
		return leds() - 1 - led;
	}

}