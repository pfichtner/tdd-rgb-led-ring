package rgbring.decorates;

import rgbring.AbstractRgbRing;
import rgbring.color.Color;

public class PeakHold extends AbstractRgbDelegate {

	protected int peakLed = -1;
	protected int peakCounter;
	private int timesToHoldPeek;

	public PeakHold(AbstractRgbRing delegate) {
		this(delegate, 3);
	}

	public PeakHold(AbstractRgbRing delegate, int timesToHoldPeek) {
		super(delegate);
		this.timesToHoldPeek = timesToHoldPeek;
	}

	@Override
	public void setLevel(int level) {
		super.setLevel(level);
		peakCounter++;
		drawPeak();
	}

	@Override
	public void setColor(int led, int r, int g, int b) {
		if (isPeakEnd()) {
			peakEnd();
		}
		if (isColored(r, g, b) && led > peakLed) {
			peakInit(led);
		}
		delegate().setColor(led, r, g, b);
	}

	protected void drawPeak() {
		if (peakLed > 0) {
			Color peakColor = getColorSchema().getColor(peakLed);
			delegate().setColor(peakLed, peakColor.r(), peakColor.g(), peakColor.b());
		}
	}

	protected boolean isPeakEnd() {
		return this.peakCounter > timesToHoldPeek;
	}

	protected void peakInit(int led) {
		this.peakCounter = 0;
		this.peakLed = led;
	}

	protected void peakEnd() {
		this.peakLed = -1;
	}

	protected void drawPeakLed(int led) {
		Color peakColor = getColorSchema().getColor(led);
		delegate().setColor(led, peakColor.r(), peakColor.g(), peakColor.b());
	}

	private boolean isColored(int r, int g, int b) {
		return !Color.BLACK.equals(new Color(r, g, b));
	}

}