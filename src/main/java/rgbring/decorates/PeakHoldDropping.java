package rgbring.decorates;

import rgbring.AbstractRgbRing;

public final class PeakHoldDropping extends PeakHold {

	public PeakHoldDropping(AbstractRgbRing delegate) {
		super(delegate);
	}

	public PeakHoldDropping(AbstractRgbRing delegate, int timesToHoldPeek) {
		super(delegate, timesToHoldPeek);
	}

	@Override
	protected void peakEnd() {
		if (peakLed >= 0) {
			peakLed--;
			peakCounter--;
		}
	}

}