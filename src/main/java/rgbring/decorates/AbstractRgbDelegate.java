package rgbring.decorates;

import rgbring.AbstractRgbRing;

public abstract class AbstractRgbDelegate extends AbstractRgbRing {

	private final AbstractRgbRing delegate;

	public AbstractRgbDelegate(AbstractRgbRing delegate) {
		super(delegate.leds(), delegate.getColorSchema());
		this.delegate = delegate;
	}

	protected AbstractRgbRing delegate() {
		return delegate;
	}

}
