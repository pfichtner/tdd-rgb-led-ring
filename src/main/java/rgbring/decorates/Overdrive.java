package rgbring.decorates;

import rgbring.AbstractRgbRing;
import rgbring.color.Color;

public final class Overdrive extends AbstractRgbDelegate {

	private final int overdriveLevel;
	private final Color overdriveColor;

	public Overdrive(AbstractRgbRing delegate, int overdriveLevel, Color overdriveColor) {
		super(delegate);
		this.overdriveLevel = overdriveLevel;
		this.overdriveColor = overdriveColor;
	}

	@Override
	public void setLevel(int level) {
		if (level >= this.overdriveLevel) {
			setAllLeds(this.overdriveColor);
		} else {
			delegate().setLevel(level * 100 / (this.overdriveLevel - 1));
		}
	}

	private void setAllLeds(Color color) {
		for (int led = 0; led < leds(); led++) {
			setColor(led, color.r(), color.g(), color.b());
		}
	}

	@Override
	public void setColor(int led, int r, int g, int b) {
		delegate().setColor(led, r, g, b);
	}

}