package rgbring;

import static rgbring.color.Color.BLACK;

import rgbring.color.Color;
import rgbring.color.ColorSchema;

public abstract class AbstractRgbRing {

	private static final int VALUE_MAX = 100;
	private static final Color OFF = BLACK;

	private final int leds;
	private final ColorSchema colorSchema;

	public AbstractRgbRing(int leds, ColorSchema colorSchema) {
		this.leds = leds;
		this.colorSchema = colorSchema;
	}

	public ColorSchema getColorSchema() {
		return colorSchema;
	}

	public int leds() {
		return leds;
	}

	public void setLevel(int level) {
		for (int led = 0; led < leds; led++) {
			Color color = isColored(led, level) ? colorSchema.getColor(led) : OFF;
			setColor(led, color.r(), color.g(), color.b());
		}
	}

	private boolean isColored(int led, int level) {
		return level > VALUE_MAX / leds * led;
	}

	public abstract void setColor(int led, int r, int g, int b);

}