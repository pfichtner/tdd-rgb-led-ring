package rgbring.mqtt;

import static java.lang.String.format;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import rgbring.strip.Strip;

public final class MqttStrip implements Strip {

	private final MqttClient client;
	private final int length;

	private final String topic = "some/led/%s/rgb";

	public MqttStrip(int length, String host, int port) throws MqttException {
		this.length = length;
		this.client = new MqttClient("tcp://" + host + ":" + port, "sliderdemo");
		this.client.connect();
	}

	@Override
	public void setColor(int led, int r, int g, int b) {
		try {
			this.client.publish(format(this.topic, led), message(r, g, b));
		} catch (MqttException e) {
			throw new RuntimeException(e);
		}
	}

	private MqttMessage message(int r, int g, int b) {
		return new MqttMessage(format("#%02X%02X%02X", r, g, b).getBytes());
	}

	@Override
	public int leds() {
		return length;
	}

}