package rgbring.strip;

public final class SubStrip implements Strip {

	private final Strip delegate;
	private final int offset;
	private final int length;

	public SubStrip(Strip delegate, int offset) {
		this(delegate, offset, delegate.leds() - offset);
	}

	public SubStrip(Strip delegate, int offset, int length) {
		this.delegate = delegate;
		this.offset = offset;
		this.length = length;
	}

	@Override
	public int leds() {
		return length;
	}

	@Override
	public void setColor(int led, int r, int g, int b) {
		delegate.setColor(led + offset, r, g, b);
	}

}