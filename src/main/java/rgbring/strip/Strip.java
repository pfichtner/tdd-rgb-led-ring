package rgbring.strip;

public interface Strip {

	void setColor(int led, int r, int g, int b);
	
	int leds();

}