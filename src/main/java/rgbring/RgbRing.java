package rgbring;

import static rgbring.color.Color.GREEN;
import static rgbring.color.Color.ORANGE;
import static rgbring.color.Color.YELLOW;

import rgbring.color.AutomaticColorSchema;
import rgbring.color.ColorSchema;
import rgbring.strip.Strip;

public class RgbRing extends AbstractRgbRing {

	private final Strip strip;

	public RgbRing(Strip strip) {
		this(strip, new AutomaticColorSchema(strip.leds(), GREEN, YELLOW, ORANGE));
	}

	public RgbRing(Strip strip, ColorSchema colorSchema) {
		super(strip.leds(), colorSchema);
		this.strip = strip;
	}

	@Override
	public void setColor(int led, int r, int g, int b) {
		strip.setColor(led, r, g, b);
	}

}