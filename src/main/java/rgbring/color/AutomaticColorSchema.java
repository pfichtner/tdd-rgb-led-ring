package rgbring.color;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class AutomaticColorSchema implements ColorSchema {

	private final Color[] availableColors;
	private final Color[] ledColors;

	public AutomaticColorSchema(int leds, Color... availableColors) {
		this.availableColors = availableColors.clone();
		this.ledColors = makeColors(leds);
	}

	@Override
	public Color getColor(int led) {
		return ledColors[led];
	}

	private Color[] makeColors(int leds) {
		Color[] colors = new Color[leds];
		for (int led = 0; led < leds; led++) {
			colors[led] = makeColor(leds, led);
		}
		return colors;
	}

	private Color makeColor(int leds, int led) {
		return availableColors[min(led / ledsPerSegment(leds), availableColors.length - 1)];
	}

	public int ledsPerSegment(int leds) {
		return max(1, leds / availableColors.length);
	}

}