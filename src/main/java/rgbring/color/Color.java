package rgbring.color;

import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isPublic;
import static java.lang.reflect.Modifier.isStatic;

import java.lang.reflect.Field;

public final class Color {

	public static final Color BLACK = new Color(0, 0, 0);

	public static final Color WHITE = new Color(255, 255, 255);

	public static final Color RED = new Color(255, 0, 0);

	public static final Color GREEN = new Color(0, 255, 0);

	public static final Color YELLOW = new Color(255, 255, 0);

	public static final Color ORANGE = new Color(255, 153, 0);

	private final int bgrValue;

	public Color(int r, int g, int b) {
		bgrValue = (b << 16) + (g << 8) + r;
	}

	public int r() {
		return bgrValue & 0x0000FF;
	}

	public int g() {
		return (bgrValue & 0x00FF00) >> 8;
	}

	public int b() {
		return (bgrValue & 0xFF0000) >> 16;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bgrValue;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Color other = (Color) obj;
		if (bgrValue != other.bgrValue)
			return false;
		return true;
	}

	@Override
	public String toString() {
		String name = namedColor();
		return name == null ? "Color [r=" + r() + ", g=" + g() + ", b=" + b() + "]" : name;
	}

	private String namedColor() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				int mod = field.getModifiers();
				if (Color.class.isAssignableFrom(field.getType()) && isPublic(mod) && isStatic(mod) && isFinal(mod)) {
					Color other = (Color) field.get(null);
					if (other.equals(this)) {
						return field.getName();
					}
				}

			}
		} catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

}