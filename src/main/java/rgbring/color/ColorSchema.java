package rgbring.color;

public interface ColorSchema {
	Color getColor(int led);
}
