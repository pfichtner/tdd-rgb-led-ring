package rgbring.color;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class ArrayColorSchema implements ColorSchema {

	public static class Builder {

		private final List<Color> colors = new ArrayList<>();

		public Builder and(int times, Color color) {
			return add(times, color);
		}

		public Builder add(int times, Color color) {
			IntStream.range(0, times).mapToObj(i -> color).forEach(colors::add);
			return this;
		}

		public ArrayColorSchema build() {
			return new ArrayColorSchema(colors.toArray(new Color[colors.size()]));
		}

	}

	private final Color[] colors;

	public static Builder builder() {
		return new Builder();
	}

	public ArrayColorSchema(Color... colors) {
		this.colors = colors.clone();
	}

	public int size() {
		return colors.length;
	}

	@Override
	public Color getColor(int led) {
		return colors[led];
	}

}