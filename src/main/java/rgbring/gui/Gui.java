package rgbring.gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.Font.ITALIC;
import static javax.swing.BoxLayout.PAGE_AXIS;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static javax.swing.SwingConstants.HORIZONTAL;
import static javax.swing.SwingUtilities.invokeAndWait;
import static rgbring.color.Color.RED;

import java.awt.Font;
import java.lang.reflect.InvocationTargetException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeListener;

import org.eclipse.paho.client.mqttv3.MqttException;

import rgbring.AbstractRgbRing;
import rgbring.RgbRing;
import rgbring.decorates.Overdrive;
import rgbring.decorates.PeakHold;
import rgbring.decorates.PeakHoldDropping;
import rgbring.decorates.Swapped;
import rgbring.mqtt.MqttStrip;
import rgbring.strip.Strip;
import rgbring.strip.SubStrip;

public class Gui extends JPanel {

	private static final long serialVersionUID = 8074193892837305515L;

	private static final int MIN = 0;
	private static final int MAX = 100;
	private static final int INIT = MIN;

	public static void main(String[] args) throws InvocationTargetException, InterruptedException {
		invokeAndWait(Gui::createAndShowGui);
	}

	private static void createAndShowGui() {
		try {
			JFrame frame = new JFrame("RGB rings via mqtt");
			frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
			frame.add(new Gui(), CENTER);
			frame.pack();
			frame.setVisible(true);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Gui() throws MqttException {
		Strip strip = new MqttStrip(32, "mqtt.local", 1883);
		setLayout(new BoxLayout(this, PAGE_AXIS));
		add(makeLabel("CPU"));
		add(makeSlider(listener(overdrive(peakHold(new RgbRing(new SubStrip(strip, 0, 16)))))));
		add(makeLabel("Memory"));
		add(makeSlider(listener(overdrive(peakHold(new Swapped(new RgbRing(new SubStrip(strip, 16))))))));
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
	}

	private PeakHold peakHold(AbstractRgbRing delegate) {
		return new PeakHoldDropping(delegate, 10);
	}

	private Overdrive overdrive(AbstractRgbRing delegate) {
		return new Overdrive(delegate, 91, RED);
	}

	private ChangeListener listener(AbstractRgbRing ring) {
		return e -> ring.setLevel(((JSlider) e.getSource()).getValue());
	}

	private JSlider makeSlider(ChangeListener changeListener) {
		JSlider slider = new JSlider(HORIZONTAL, MIN, MAX, INIT);
		slider.addChangeListener(changeListener);
		configure(slider);
		return slider;
	}

	private JLabel makeLabel(String text) {
		JLabel label = new JLabel(text, SwingConstants.CENTER);
		label.setAlignmentX(CENTER_ALIGNMENT);
		return label;
	}

	private void configure(JSlider slider) {
		slider.setMajorTickSpacing(10);
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
		slider.setFont(new Font("Serif", ITALIC, 15));
	}

}
